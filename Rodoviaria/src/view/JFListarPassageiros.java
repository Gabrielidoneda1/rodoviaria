package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import model.bean.Passageiro;
import model.dao.PassageiroDAO;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class JFListarPassageiros extends JFrame {

	private JPanel contentPane;
	private JTable JTpassageiros;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					JFListarPassageiros frame = new JFListarPassageiros();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public JFListarPassageiros() {
		setTitle("Listar Passageiros");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 603, 372);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Listar Passageiros:");
		lblNewLabel.setFont(new Font("Source Serif Pro Black", Font.BOLD, 17));
		lblNewLabel.setBounds(10, 11, 174, 25);
		contentPane.add(lblNewLabel);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 32, 577, 226);
		contentPane.add(scrollPane);
		 
		JTpassageiros = new JTable();
		JTpassageiros.setModel(new DefaultTableModel(
			new Object[][] {
				{null, null, null, null, null, null, null, null},
				{null, null, null, null, null, null, null, null},
			},
			new String[] {
				"Id_p", "Nome", "G\u00EAnero", "Rg", "Cpf", "Endere\u00E7o", "Email", "Telefone"
			}
		));
		scrollPane.setViewportView(JTpassageiros);
		
		JButton CadastrarPassageiro = new JButton("Cadastrar Passageiro");
		CadastrarPassageiro.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JFCadastrarPassageiro cp = new JFCadastrarPassageiro();
				cp.setVisible(true);
			}
		});
		CadastrarPassageiro.setBounds(10, 277, 141, 23);
		contentPane.add(CadastrarPassageiro);
		
		JButton AlterarPassageiros = new JButton("Alterar Passageiros");
		AlterarPassageiros.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(JTpassageiros.getSelectedRow()!= -1) {
					JFAtualizarPassageiro ap = new JFAtualizarPassageiro((int)JTpassageiros.getValueAt(JTpassageiros.getSelectedRow(),0));
					ap.setVisible(true);
				}else {
					JOptionPane.showMessageDialog(null, "Selecione um filme!");
				}
				readJTable();			
			}
		});
		AlterarPassageiros.setBounds(165, 277, 131, 23);
		contentPane.add(AlterarPassageiros);
		
		JButton ExcluirPassageiro = new JButton("Excluir Passageiro");
		ExcluirPassageiro.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(JTpassageiros.getSelectedRow()!= -1) {
				   int opcao = JOptionPane.showConfirmDialog(null, "Deseja excluir o passageiro selecionado?", "Exclus�o", JOptionPane.YES_NO_OPTION); 
				     if(opcao == 0) {
					       PassageiroDAO dao = new PassageiroDAO();
					       Passageiro p = new Passageiro();
					       p.setIdpassageiro((int)JTpassageiros.getValueAt(JTpassageiros.getSelectedRow(),0 ));
					       dao.delete(p);
				     }
			    }else {
			    	JOptionPane.showMessageDialog(null, "Selecione um passageiro");
			    }
				readJTable();
			}
		});
		ExcluirPassageiro.setBounds(328, 277, 119, 23);
		contentPane.add(ExcluirPassageiro);
		readJTable();
	}
	
	public void readJTable() {
		DefaultTableModel modelo = (DefaultTableModel) JTpassageiros.getModel();
		modelo.setNumRows(0);
		PassageiroDAO pdao = new PassageiroDAO();
		for(Passageiro p : pdao.read()) {
			modelo.addRow(new Object[] {
				p.getIdpassageiro(),
				p.getNome(),
				p.getGenero(),
				p.getRg(),
				p.getCpf(),
				p.getEndereco(),
				p.getEmail(),
				p.getTelefone()
			});
		}
	}
}
