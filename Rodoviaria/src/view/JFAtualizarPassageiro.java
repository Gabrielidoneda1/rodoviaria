package view;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.AbstractButton;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.text.JTextComponent;

import model.bean.Passageiro;
import model.dao.PassageiroDAO;

import javax.swing.JLabel;
import javax.swing.JTextField;

public class JFAtualizarPassageiro extends JFrame {
	private static final String textTelefone = null; 
	private JTextField txtNome;
	private JTextField textGenero;
	private JTextField textRg;
	private JTextField textCPF;
	private JTextField textEndereco;
	private JTextField textEmail;
	private JTextField textField;
	private static int id;


	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					JFAtualizarPassageiro frame = new JFAtualizarPassageiro(id);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 * @param id2 
	 */
	public JFAtualizarPassageiro(int id2) {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 563, 408);
		getContentPane().setLayout(null);
		getContentPane().setLayout(null);
		
		PassageiroDAO pdao = new PassageiroDAO();
		Passageiro p = pdao.read(id2);
		
		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setBounds(10, 11, 46, 14);
		getContentPane().add(lblNewLabel);
		
		JLabel lblID = new JLabel(String.valueOf(p.getIdpassageiro()));
		lblID.setBounds(439, 11, 75, 20);
		getContentPane().add(lblID);
		
		JLabel lblNome = new JLabel("Nome:");
		lblNome.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 12));
		lblNome.setBounds(10, 52, 46, 14);
		getContentPane().add(lblNome);
		
		txtNome = new JTextField();
		txtNome.setBounds(10, 68, 514, 20);
		getContentPane().add(txtNome);
		txtNome.setColumns(10);
		
		JLabel lblGenero = new JLabel("G\u00EAnero:");
		lblGenero.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 12));
		lblGenero.setBounds(10, 96, 59, 14);
		getContentPane().add(lblGenero);
		
		textGenero = new JTextField();
		textGenero.setBounds(10, 112, 514, 20);
		getContentPane().add(textGenero);
		textGenero.setColumns(10);
		
		JLabel lblRegistroGeral = new JLabel("Registro Geral:");
		lblRegistroGeral.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 12));
		lblRegistroGeral.setBounds(10, 143, 133, 14);
		getContentPane().add(lblRegistroGeral);
		
		textRg = new JTextField();
		textRg.setText("");
		textRg.setBounds(10, 161, 514, 20);
		getContentPane().add(textRg);
		textRg.setColumns(10);
		
		JLabel lblCpf = new JLabel("CPF:");
		lblCpf.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 12));
		lblCpf.setBounds(10, 189, 59, 14);
		getContentPane().add(lblCpf);
		
		textCPF = new JTextField();
		textCPF.setBounds(10, 205, 514, 20);
		getContentPane().add(textCPF);
		textCPF.setColumns(10);
		
		JLabel lblEndereco = new JLabel("Endere\u00E7o:");
		lblEndereco.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 12));
		lblEndereco.setBounds(10, 236, 133, 14);
		getContentPane().add(lblEndereco);
		
		textEndereco = new JTextField();
		textEndereco.setBounds(10, 251, 514, 20);
		getContentPane().add(textEndereco);
		textEndereco.setColumns(10);
		
		JLabel lblEmail = new JLabel("E-mail:");
		lblEmail.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 12));
		lblEmail.setBounds(10, 282, 121, 14);
		getContentPane().add(lblEmail);
		
		textEmail = new JTextField();
		textEmail.setBounds(59, 282, 465, 20);
		getContentPane().add(textEmail);
		textEmail.setColumns(10);
		
		JLabel lblTelefone = new JLabel("Telefone:");
		lblTelefone.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 12));
		lblTelefone.setBounds(10, 313, 121, 14);
		getContentPane().add(lblTelefone);
		
		textField = new JTextField();
		textField.setText("");
		textField.setBounds(69, 311, 455, 20);
		getContentPane().add(textField);
		textField.setColumns(10);
		txtNome.setText(p.getNome());
		((JTextComponent) textGenero).setText(String.valueOf(p.getGenero()));
		textRg.setText(String.valueOf(p.getRg()));
		textCPF.setText(String.valueOf(p.getCpf()));
		textEndereco.setText(String.valueOf(p.getEndereco()));
		textEmail.setText(p.getEmail());
		textField.setText(String.valueOf(p.getTelefone()));
		
		
		JButton btnAlterar = new JButton("Alterar");
		btnAlterar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Passageiro p = new Passageiro(); 
				PassageiroDAO dao = new PassageiroDAO();
				p.setIdpassageiro(Integer.parseInt(lblID.getText()));
				p.setNome(txtNome.getText());
				p.setGenero(textGenero.getText());
				p.setRg(textRg.getText());
				p.setCpf(textCPF.getText());
				p.setEndereco(textEndereco.getText());
			    p.setEmail(textEmail.getText());
			    p.setTelefone(textField.getText());
			    
			    dao.update(p); 
			    
			    dispose();
				
			}
		});
		btnAlterar.setBounds(10, 338, 89, 23);
		getContentPane().add(btnAlterar);
		
		JButton btnLimpar = new JButton("Limpar");
		btnLimpar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				txtNome.setText(null);
				textGenero.setText(null);
				textRg.setText(null);
				textCPF.setText(null);
				textEndereco.setText(null);
				textEmail.setText(null);
				textField.setText(null);
			}
		});
		btnLimpar.setBounds(148, 338, 89, 23);
		getContentPane().add(btnLimpar);
		
		JButton btnCancelar = new JButton("Cancelar");
		btnCancelar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		btnCancelar.setBounds(287, 338, 89, 23);
		getContentPane().add(btnCancelar);
		
		JLabel lblNewLabel_1 = new JLabel("Atualizar Passageiro");
		lblNewLabel_1.setBounds(20, 11, 142, 20);
		getContentPane().add(lblNewLabel_1);
		
		JLabel lblId = new JLabel("ID passageiro:");
		lblId.setBounds(354, 11, 75, 14);
		getContentPane().add(lblId);
	}
}
