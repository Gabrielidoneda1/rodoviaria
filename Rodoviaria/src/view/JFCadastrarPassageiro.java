package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import model.bean.Passageiro;
import model.dao.PassageiroDAO;

import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class JFCadastrarPassageiro extends JFrame {

	private JPanel contentPane;
	private JTextField txtNome;
	private JTextField textGenero;
	private JTextField textRg;
	private JTextField textCPF;
	private JTextField textEndereco;
	private JTextField textEmail;
	private JTextField textField;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					JFCadastrarPassageiro frame = new JFCadastrarPassageiro();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public JFCadastrarPassageiro() {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 563, 408);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblCadastrarPassageiro = new JLabel("Cadastrar Passageiro");
		lblCadastrarPassageiro.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblCadastrarPassageiro.setBounds(10, 27, 227, 14);
		contentPane.add(lblCadastrarPassageiro);
		
		JLabel lblNome = new JLabel("Nome:");
		lblNome.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 12));
		lblNome.setBounds(10, 52, 46, 14);
		contentPane.add(lblNome);
		
		txtNome = new JTextField();
		txtNome.setBounds(10, 68, 514, 20);
		contentPane.add(txtNome);
		txtNome.setColumns(10);
		
		JLabel lblGenero = new JLabel("G\u00EAnero:");
		lblGenero.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 12));
		lblGenero.setBounds(10, 96, 59, 14);
		contentPane.add(lblGenero);
		
		textGenero = new JTextField();
		textGenero.setBounds(10, 112, 514, 20);
		contentPane.add(textGenero);
		textGenero.setColumns(10);
		
		JLabel lblRegistroGeral = new JLabel("Registro Geral:");
		lblRegistroGeral.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 12));
		lblRegistroGeral.setBounds(10, 143, 133, 14);
		contentPane.add(lblRegistroGeral);
		
		textRg = new JTextField();
		textRg.setText("");
		textRg.setBounds(10, 161, 514, 20);
		contentPane.add(textRg);
		textRg.setColumns(10);
		
		JLabel lblCpf = new JLabel("CPF:");
		lblCpf.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 12));
		lblCpf.setBounds(10, 189, 59, 14);
		contentPane.add(lblCpf);
		
		textCPF = new JTextField();
		textCPF.setBounds(10, 205, 514, 20);
		contentPane.add(textCPF);
		textCPF.setColumns(10);
		
		JLabel lblEndereco = new JLabel("Endere\u00E7o:");
		lblEndereco.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 12));
		lblEndereco.setBounds(10, 236, 133, 14);
		contentPane.add(lblEndereco);
		
		textEndereco = new JTextField();
		textEndereco.setBounds(10, 251, 514, 20);
		contentPane.add(textEndereco);
		textEndereco.setColumns(10);
		
		JLabel lblEmail = new JLabel("E-mail:");
		lblEmail.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 12));
		lblEmail.setBounds(10, 282, 121, 14);
		contentPane.add(lblEmail);
		
		textEmail = new JTextField();
		textEmail.setBounds(59, 282, 465, 20);
		contentPane.add(textEmail);
		textEmail.setColumns(10);
		
		JLabel lblTelefone = new JLabel("Telefone:");
		lblTelefone.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 12));
		lblTelefone.setBounds(10, 313, 121, 14);
		contentPane.add(lblTelefone);
		
		textField = new JTextField();
		textField.setText("");
		textField.setBounds(69, 311, 455, 20);
		contentPane.add(textField);
		textField.setColumns(10);
		
		JButton btnCadastrar = new JButton("Cadastrar");
		btnCadastrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Passageiro p = new Passageiro(); 
				PassageiroDAO dao = new PassageiroDAO();
				
				p.setNome(txtNome.getText()); 
				p.setGenero(textGenero.getText());
				p.setRg(textRg.getText());
				p.setCpf(textCPF.getText());
				p.setEndereco(textEndereco.getText());
			    p.setEmail(textEmail.getText());
			    p.setTelefone(textField.getText());
			    
			    dao.create(p); 
			    
			    dispose();
				
			}
		});
		btnCadastrar.setBounds(10, 338, 89, 23);
		contentPane.add(btnCadastrar);
		
		JButton btnLimpar = new JButton("Limpar");
		btnLimpar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				txtNome.setText(null);
				textGenero.setText(null);
				textRg.setText(null);
				textCPF.setText(null);
				textEndereco.setText(null);
				textEmail.setText(null);
				textField.setText(null);
				
			}
		});
		btnLimpar.setBounds(148, 338, 89, 23);
		contentPane.add(btnLimpar);
		
		JButton btnCancelar = new JButton("Cancelar");
		btnCancelar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		btnCancelar.setBounds(287, 338, 89, 23);
		contentPane.add(btnCancelar);
	}
}
