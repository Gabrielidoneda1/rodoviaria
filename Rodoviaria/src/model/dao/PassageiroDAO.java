package model.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;

import connection.ConnectionFactory;
import model.bean.Passageiro;
 
public class PassageiroDAO {
	
	public void create(Passageiro p) {
		Connection con = ConnectionFactory.getConnection();
		PreparedStatement stmt = null;
		
		try{
			stmt = con.prepareStatement("INSERT INTO passageiro (nome, g�nero, RG, CPF,endere�o, email, telefone) VALUES (?,?,?,?,?,?,?) ");
			stmt.setString(1, p.getNome());
			stmt.setString(2, p.getGenero());
			stmt.setString(3, p.getRg());
			stmt.setString(4, p.getCpf());
			stmt.setString(5, p.getEndereco());
			stmt.setString(6, p.getEmail());
			stmt.setString(7, p.getTelefone());
			
			stmt.executeUpdate();
			JOptionPane.showMessageDialog(null, "Salvo com sucesso!");
			
		}catch (SQLException e) {
			JOptionPane.showMessageDialog(null, "Erro ao salvar" + e);
		}finally {
			ConnectionFactory.closeConnection(con, stmt, null);
		}
		
		
	}
	
	public List<Passageiro> read(){
		Connection con = ConnectionFactory.getConnection();
	    PreparedStatement stmt = null;
		ResultSet rs = null;
		List<Passageiro> passageiros = new ArrayList<>();
		
		try {
			stmt = con.prepareStatement("SELECT * FROM passageiro");
			rs = stmt.executeQuery();
			while(rs.next()) {
				Passageiro p = new Passageiro();
				p.setIdpassageiro(rs.getInt("id_passageiro"));
				p.setNome(rs.getString("nome"));
				p.setGenero(rs.getString("g�nero"));
				p.setRg(rs.getString("RG"));
				p.setCpf(rs.getString("CPF"));
				p.setEndereco(rs.getString("endere�o"));
				p.setEmail(rs.getString("email"));
				p.setTelefone(rs.getString("telefone"));
				passageiros.add(p);
			}
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, "Erro ao exibir as informa��es do BD" + e);
			e.printStackTrace();
		}finally {
			ConnectionFactory.closeConnection(con, stmt, rs);
		}
		return passageiros;
	}
	
	public void delete(Passageiro p) {
		
		Connection con = ConnectionFactory.getConnection();
		PreparedStatement stmt = null;
		
		try {
			stmt = con.prepareStatement("DELETE FROM passageiro WHERE id_passageiro=?");
			stmt.setInt(1,p.getIdpassageiro() );
			stmt.executeUpdate();
			
			JOptionPane.showMessageDialog(null, "Passageiro excluido com sucesso!");	
		}catch (SQLException e) {
			JOptionPane.showMessageDialog(null, "Erro ao excluir: "+e);
		}finally {
			ConnectionFactory.closeConnection(con, stmt);
		}		
	
	}
	
	public Passageiro read(int id) {
		 Connection con = ConnectionFactory.getConnection();
		 PreparedStatement stmt = null;
		 ResultSet rs = null;
		 Passageiro p = new Passageiro();
		 
		 try {
			stmt = con.prepareStatement("SELECT * FROM passageiro WHERE id_passageiro=? LIMIT 1;");
			stmt.setInt(1, id);
			rs = stmt.executeQuery();
			if(rs != null && rs.next()) {
				p.setIdpassageiro(rs.getInt("id_passageiro"));
				p.setNome(rs.getString("nome"));
				p.setCpf(rs.getString("CPF"));
				p.setEmail(rs.getString("email"));
				p.setEndereco(rs.getString("endere�o"));
				p.setGenero(rs.getString("g�nero"));
				p.setRg(rs.getString("RG"));
				p.setTelefone(rs.getString("telefone"));
				
			}	
		 } catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
		 }finally {
			 ConnectionFactory.closeConnection(con, stmt, rs);
		 }
		 return p;	
	}
	
	public void update(Passageiro p) {
		Connection con = ConnectionFactory.getConnection();
		PreparedStatement stmt = null;
		
		try{
			stmt = con.prepareStatement("UPDATE passageiro SET nome=?, g�nero=?, RG=?, CPF=?,endere�o=?, email=?, telefone=? WHERE id_passageiro=?; ");
			stmt.setString(1, p.getNome());
			stmt.setString(2, p.getGenero());
			stmt.setString(3, p.getRg());
			stmt.setString(4, p.getCpf());
			stmt.setString(5, p.getEndereco());
			stmt.setString(6, p.getEmail());
			stmt.setString(7, p.getTelefone());
			stmt.setInt(8,p.getIdpassageiro());
			stmt.executeUpdate();
			
			stmt.executeUpdate();
			JOptionPane.showMessageDialog(null, "Aletarado com sucesso!");
			
		}catch (SQLException e) {
			JOptionPane.showMessageDialog(null, "Erro ao alterar" + e);
		}finally {
			ConnectionFactory.closeConnection(con, stmt, null);
		}
		
		
	}
	
	
	
}
      

