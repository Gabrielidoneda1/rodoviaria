package model.bean;

public class Passageiro {
     private int idpassageiro;
     private String nome;
     private String genero;
     private String rg;
     private String cpf;
     private String endereco;
     private String email;
     private String telefone;
     
	public int getIdpassageiro() {
		return idpassageiro;
	}
	public void setIdpassageiro(int idpassageiro) {
		this.idpassageiro = idpassageiro;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getGenero() {
		return genero;
	}
	public void setGenero(String genero) {
		this.genero = genero;
	}
	public String getRg() {
		return rg; 
	}
	public void setRg(String string) {
		this.rg = string;
	}
	public String getCpf() {
		return cpf;
	}
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	public String getEndereco() {
		return endereco;
	}
	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getTelefone() {
		return telefone;
	}
	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}
     
    
}
